﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GameManagment_VeselaGeorgieva.Models;
using System.Security.Claims;

namespace GameManagment_VeselaGeorgieva.Controllers
{
    [Authorize]
    public class GamesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Games
        public ActionResult Index()
        {
            var claimsIdentity = User.Identity as ClaimsIdentity;
            var userIdValue = ""; 
            if (claimsIdentity != null)
            {
                var userIdClaim = claimsIdentity.Claims
                    .FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier);

                if (userIdClaim != null)
                {
                     userIdValue = userIdClaim.Value;
                }
            }
            var loginUser = db.Users.Where(u => u.Id == userIdValue).FirstOrDefault();            
            var languageId = loginUser.LanguageId;
                var gamesList = (from games in db.Games
                        from gameLanguages in db.GameLanguages
                        where games.Id == gameLanguages.GameId
                        && gameLanguages.LanguageId == languageId
                        select (games)).Include(g => g.Suite);
            return View(gamesList.ToList());
        }

        // GET: Games/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Game game = db.Games.Include(g => g.Suite).Where(g => g.Id == id).FirstOrDefault();
            //Game game = db.Games.Find(id);
            if (game == null)
            {
                return HttpNotFound();
            }
            return View(game);
        }
        
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
