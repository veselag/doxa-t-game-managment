﻿using GameManagment_VeselaGeorgieva.Models;
using GameManagment_VeselaGeorgieva.XMLModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace GameManagment_VeselaGeorgieva.Controllers.API
{
    public class GameToUsersController : ApiController
    {
        private ApplicationDbContext _context;
        public GameToUsersController()
        {
            _context = new ApplicationDbContext();
        }

        public ICollection<GameWithUsersXML> GetGamesToUsers() 
        {
            try
            {
                var gamesWithUsers = new List<GameWithUsersXML>();
                var games = _context.Games.ToList();
                foreach (var game in games)
                {
                    var gameWithUsers = new GameWithUsersXML();
                    gameWithUsers.Languages = new List<LanguageXML>();
                    gameWithUsers.Users = new List<UserXML>();
                    gameWithUsers.Game = new GameXML { Id = game.Id, GameId = game.GameId, Name = game.Name, SuiteID = game.SuiteID };
                    var suite = _context.Suites.Find(game.SuiteID);
                    gameWithUsers.Suite = new SuiteXML { Id = suite.Id, Name = suite.Name, SuiteId = suite.SuiteId };
                    AddLanguages(game, gameWithUsers);
                    AddUsers(gamesWithUsers, game, gameWithUsers);
                }

                return gamesWithUsers;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        private void AddUsers(List<GameWithUsersXML> gamesWithUsers, Game game, GameWithUsersXML gameWithUsers)
        {
            var userList = (from users in _context.Users
                            from language in _context.Languages
                            from gameLanguages in _context.GameLanguages
                            where gameLanguages.GameId == game.Id
                            && users.LanguageId == language.Id
                            && language.Id == gameLanguages.LanguageId
                            select (users)).ToList();
            foreach (var user in userList)
            {

                var userXML = new UserXML { Id = user.Id, UserName = user.UserName };
                gameWithUsers.Users.Add(userXML);
            }

            gamesWithUsers.Add(gameWithUsers);
        }

        private void AddLanguages(Game game, GameWithUsersXML gameWithUsers)
        {
            var languageList = (from gameLanguages in _context.GameLanguages
                                from language in _context.Languages
                                where gameLanguages.GameId == game.Id
                                && language.Id == gameLanguages.LanguageId
                                select (language)).ToList();
            foreach (var language in languageList)
            {
                var languageXML = new LanguageXML { Id = language.Id, Name = language.Name };
                gameWithUsers.Languages.Add(languageXML);
            }
        }
    }
}
