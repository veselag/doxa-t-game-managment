﻿using GameManagment_VeselaGeorgieva.Models;
using GameManagment_VeselaGeorgieva.XMLModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web.Http;

namespace GameManagment_VeselaGeorgieva.Controllers.API
{
    public class GamesController : ApiController
    {
        private ApplicationDbContext _context;
        //private readonly string gamesURL = "https://21bet-gp3.discreetgaming.com/gamelist.do?bankId=1951";
        private readonly string gamesURL = "https://cdn2.21bet.com/games/gamelist.xml";
        public GamesController()
        {
            _context = new ApplicationDbContext();
        }

        private GAMESSUITES GetGames()
        {
            try
            {
                var client = new HttpClient();
                var response = client.GetAsync(gamesURL).Result;
                var result = response.Content.ReadAsStringAsync().Result;
                var games = Deserialize(result, typeof(GAMESSUITES));
                return games;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private GAMESSUITES Deserialize(string xml, Type toType)
        {
            try
            {
                using (MemoryStream memoryStream = new MemoryStream(Encoding.UTF8.GetBytes(xml)))
                {
                    StreamReader str = new StreamReader(memoryStream);
                    System.Xml.Serialization.XmlSerializer xSerializer = new System.Xml.Serialization.XmlSerializer(toType);
                    return (GAMESSUITES)xSerializer.Deserialize(str);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        public ResultXML StoreGames()
        {
            var message = new ResultXML();
            try
            {
                var gamessuites = GetGames();
                StoreLangiages(gamessuites);
                foreach (var suite in gamessuites.SUITES)
                {
                    var suiteModel = new Suite
                    {
                        SuiteId = suite.ID,
                        Name = suite.NAME
                    };
                    var suiteAdded = _context.Suites.Add(suiteModel);
                    _context.SaveChanges();
                    SaveGames(suite, suiteAdded);
                }
                message.Message = "The games was stored successful in DataBase";
                return message;
            }
            catch (Exception ex)
            {
                message.Message = string.Format("The games saving was unsuccessful {0}", ex.Message); ;
                return message;
            }
        }

        private void SaveGames(GAMESSUITESSUITE suite, Suite suiteAdded)
        {
            try
            {
                var games = new List<Game>();

                foreach (var game in suite.GAMES)
                {
                    var gameMode = new Game
                    {
                        GameId = game.ID,
                        Name = game.NAME,
                        ImageUrl = game.IMAGEURL,
                        SuiteID = suiteAdded.Id
                    };
                    var gameAdded = _context.Games.Add(gameMode);
                    _context.SaveChanges();
                    var languagesForOneGame = game.LANGUAGES.Split(',').ToList();
                    SaveLanguages(gameAdded, languagesForOneGame);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        private void SaveLanguages(Game gameAdded, List<string> languagesForOneGame)
        {
            try
            {
                foreach (var language in languagesForOneGame)
                {
                    var langiageInDB = _context.Languages.FirstOrDefault(l => l.Name == language);
                    if (langiageInDB != null)
                    {
                        var gameLanguage = new GameLanguage
                        {
                            GameId = gameAdded.Id,
                            LanguageId = langiageInDB.Id
                        };
                        _context.GameLanguages.Add(gameLanguage);
                        _context.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void StoreLangiages(GAMESSUITES games)
        {
            try
            {
                var languages = new List<string>();
                foreach (var suite in games.SUITES)
                {
                    foreach (var game in suite.GAMES)
                    {
                        var languagesForOneGame = game.LANGUAGES.Split(',').ToList();
                        foreach (var langiage in languagesForOneGame)
                        {
                            if (!languages.Contains(langiage))
                            {
                                languages.Add(langiage);
                                _context.Languages.Add(new Language { Name = langiage });
                                _context.SaveChanges();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}

        


