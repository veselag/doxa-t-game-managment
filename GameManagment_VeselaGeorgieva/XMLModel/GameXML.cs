﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace GameManagment_VeselaGeorgieva.XMLModel
{
    public class GameXML
    {
        public int Id { get; set; }
        public int GameId { get; set; }
        public string Name { get; set; }
        public string ImageUrl { get; set; }
        public int SuiteID { get; set; }
    }
}