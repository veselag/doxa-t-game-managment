﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GameManagment_VeselaGeorgieva.XMLModel
{
    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class GAMESSUITES
    {

        private GAMESSUITESSUITE[] sUITESField;

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("SUITE", IsNullable = false)]
        public GAMESSUITESSUITE[] SUITES
        {
            get
            {
                return this.sUITESField;
            }
            set
            {
                this.sUITESField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class GAMESSUITESSUITE
    {

        private GAMESSUITESSUITEGAME[] gAMESField;

        private string idField;

        private string nAMEField;

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("GAME", IsNullable = false)]
        public GAMESSUITESSUITEGAME[] GAMES
        {
            get
            {
                return this.gAMESField;
            }
            set
            {
                this.gAMESField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ID
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string NAME
        {
            get
            {
                return this.nAMEField;
            }
            set
            {
                this.nAMEField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class GAMESSUITESSUITEGAME
    {

        private ushort idField;

        private string nAMEField;

        private string iMAGEURLField;

        private string lANGUAGESField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public ushort ID
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string NAME
        {
            get
            {
                return this.nAMEField;
            }
            set
            {
                this.nAMEField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string IMAGEURL
        {
            get
            {
                return this.iMAGEURLField;
            }
            set
            {
                this.iMAGEURLField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string LANGUAGES
        {
            get
            {
                return this.lANGUAGESField;
            }
            set
            {
                this.lANGUAGESField = value;
            }
        }
    }


}

