﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace GameManagment_VeselaGeorgieva.XMLModel
{
    public class SuiteXML
    {
        public int Id { get; set; }
        public string SuiteId { get; set; }
        public string Name { get; set; }
    }
}