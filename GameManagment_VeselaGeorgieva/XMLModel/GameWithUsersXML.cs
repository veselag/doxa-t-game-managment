﻿using System.Collections.Generic;

namespace GameManagment_VeselaGeorgieva.XMLModel
{
    public class GameWithUsersXML
    {
        public GameXML Game { get; set; }
        public SuiteXML Suite { get; set; }
        public ICollection<LanguageXML> Languages { get; set; }
        public ICollection<UserXML> Users { get; set; }
    }
}