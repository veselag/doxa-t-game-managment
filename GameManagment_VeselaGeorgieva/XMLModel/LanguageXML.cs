﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GameManagment_VeselaGeorgieva.XMLModel
{
    public class LanguageXML
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}