﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Web;

namespace GameManagment_VeselaGeorgieva.Extensions
{
    public static class IdentityExtensions
    {
        public static string GetLanguageId(this IIdentity identity)
        {
            var claim = ((ClaimsIdentity)identity).FindFirst("LanguageId");
            return (claim != null) ? claim.Value : string.Empty;
        }
    }
}