﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(GameManagment_VeselaGeorgieva.Startup))]
namespace GameManagment_VeselaGeorgieva
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
