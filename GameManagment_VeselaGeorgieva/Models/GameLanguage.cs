﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GameManagment_VeselaGeorgieva.Models
{
    public class GameLanguage
    {
        public int Id { get; set; }
        public int GameId { get; set; }
        public Game Game { get; set; }
        public int LanguageId { get; set; }
        public Language Language { get; set; }
    }
}