﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GameManagment_VeselaGeorgieva.Models
{
    public class Language
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<GameLanguage> GameLanguage { get; set; }
    }
}