﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace GameManagment_VeselaGeorgieva.Models
{
    public class Game
    {
        public int Id { get; set; }
        public int GameId { get; set; }
        [DisplayName("Game Name")]
        public string Name { get; set; }
        [DisplayName("Image url")]
        public string ImageUrl { get; set; }
        public int SuiteID { get; set; }
        public Suite Suite { get; set; }
        public ICollection<GameLanguage> GameLanguage { get; set; }
    }
}