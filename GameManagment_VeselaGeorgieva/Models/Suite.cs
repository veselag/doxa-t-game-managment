﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace GameManagment_VeselaGeorgieva.Models
{
    public class Suite
    {
        public int Id { get; set; }
        public string SuiteId { get; set; }
        [DisplayName("Suite Name")]
        public string Name { get; set; }
        public ICollection<Game> Games { get; set; }
    }
}